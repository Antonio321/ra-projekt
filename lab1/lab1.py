import numpy as np
import math
import time

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *



class Object:
    def __init__(self, vertices, polygons) -> None:
        self.vertices = vertices
        self.polygons = polygons
        self.edges = []
        self.transform_data()
        self.central_position = (0, 0, 0)
        # zadano u vježbi
        self.starting_direction = [0, 0, 1]
        self.angle = 0
        self.axis = [0, 0, 0]

    def transform_data(self):
        for i, vertex in enumerate(self.vertices):
            # magic number, ali potrebno je skalirati jer bi inace avion bio premalen
            #self.vertices[i] = (vertex[0], vertex[1], vertex[2])
            self.vertices[i] = (vertex[0] * 15, vertex[1] * 15, vertex[2] * 15)

        for polygon in self.polygons:
            # append vertex all possible vertex pairs
            # subtract 1 as vertices are enumerated from 1
            self.edges.append((int(polygon[0]) - 1, int(polygon[1]) - 1))
            self.edges.append((int(polygon[1]) - 1, int(polygon[2]) - 1))
            self.edges.append((int(polygon[2]) - 1, int(polygon[0]) - 1))



    def draw(self):
        # draw lines from using pair of vertices
        glBegin(GL_LINES)
        for edge in self.edges:
            for vertex in edge:
                glVertex3fv(self.vertices[vertex])
        glEnd()




class Object_loader:
    def __init__(self, file_name) -> None:
        self.file_name = file_name

    def load(self):
        lines = open(self.file_name, "r").read().split('\n')
        vertices = []
        polygons = []
        for line in lines:
            line = line.split(' ')
            if line[0] == 'v':
                vertices.append((float(line[1]), float(line[2]), float(line[3])))
            if line[0] == 'f':
                polygons.append((int(line[1]), int(line[2]), int(line[3])))

        return Object(vertices, polygons)

class Curve_loader:
    def __init__(self, file_name) -> None:
        self.file_name = file_name

    def load_control_points(self):
        file = open(self.file_name, 'r').read().split('\n')
        control_points = []
        for line in file:
            line = line.split(' ')
            control_points.append((float(line[0]), float(line[1]), float(line[2])))
            
        return control_points
        

class Curve:
    def __init__(self, control_points) -> None:
        self.control_points = control_points
        self.b_spline_points = []
        self.tangents = []
        self.axis = []
        self.interpolated_points = []
        self.position_counter = 0

        # calculate all points and print them to the screen
        self.interpolate()
        for i in self.interpolated_points:
            print(i)

    def interpolate(self):
        for i in range(1, len(self.control_points) - 2):
            t = 0
            step = 0.1
            s = (0, 0, 1)

            while(t < 1):
                b_spline_points = self.calculate_b_spline_points(t, [self.control_points[i-1], self.control_points[i], self.control_points[i+1], self.control_points[i+2]])
                tangent = self.calculate_tangent(t, [self.control_points[i-1], self.control_points[i], self.control_points[i+1], self.control_points[i+2]])
                self.tangents.append(tangent)
                #axis = self.calculate_axis()
                self.interpolated_points.append(b_spline_points)

                t += step
                s += b_spline_points

            

    def calculate_b_spline_points(self, t, points):
        t_matrix = np.array([t**3, t**2, t, 1])
        constant = 1/6
        B_matrix = np.array([[-1, 3, -3, 1], [3, -6, 3, 0], [-3, 0, 3, 0], [1, 4, 1, 0]])

        r_x = np.array([points[0][0], points[1][0], points[2][0], points[3][0]])
        r_y = np.array([points[0][1], points[1][1], points[2][1], points[3][1]])
        r_z = np.array([points[0][2], points[1][2], points[2][2], points[3][2]])


        p_x = constant *  np.matmul(np.matmul(t_matrix, B_matrix), r_x)
        p_y = constant *  np.matmul(np.matmul(t_matrix, B_matrix), r_y)
        p_z = constant *  np.matmul(np.matmul(t_matrix, B_matrix), r_z)
        
        return (p_x, p_y, p_z)

    def calculate_tangent(self, t, points):
        t_matrix = np.array([t**2, t, 1])
        constant = 1/2
        B_matrix = np.array([[-1, 3, -3, 1], [2, -4, 2, 0], [-1, 0, 1, 0]])
        r_x = np.array([points[0][0], points[1][0], points[2][0], points[3][0]])
        r_y = np.array([points[0][1], points[1][1], points[2][1], points[3][1]])
        r_z = np.array([points[0][2], points[1][2], points[2][2], points[3][2]])
        
        p_x = constant *  np.matmul(np.matmul(t_matrix, B_matrix), r_x)
        p_y = constant *  np.matmul(np.matmul(t_matrix, B_matrix), r_y)
        p_z = constant *  np.matmul(np.matmul(t_matrix, B_matrix), r_z)

        return (p_x, p_y, p_z)

    def calculate_axis(self):
        pass
        return None

    def get_next_point_and_tangent(self):
        try:
            tmp = (self.interpolated_points[self.position_counter], self.tangents[self.position_counter])
        except IndexError:
            print("Program je gotov")
            exit(0)

        self.position_counter += 1
        return tmp

    def get_next_orientation(self):
        return self.tangents[self.position_counter - 1]

    def draw(self):
        glBegin(GL_POINTS)
        for point in self.interpolated_points:
            print(point)
            glVertex3fv(point)
        glEnd()

    def draw_tangent(self, point):
        # not working!!
        
        print(point)
        glBegin(GL_LINES)
        glVertex3fv(point)
        tangent_finish = (point[0] + self.tangents[self.position_counter][0] * 2,
                    point[1] + self.tangents[self.position_counter][1] * 2,
                    point[2] + self.tangents[self.position_counter][2] * 2,
        )
        glVertex3fv(tangent_finish)
        glEnd()

class Main:
    def __init__(self) -> None:
        self.obj = None
        self.window = None
        self.curve = None

    def render_scene(self):
        # set color
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClear(GL_COLOR_BUFFER_BIT)
        glColor3f(1, 1, 1)

        # draw a curve
        glLoadIdentity()
        gluPerspective(60, 1, 1, 100)
        glTranslatef(0, -20, -100)
        glRotatef(-120, 1, 1, 1)
        self.curve.draw()

        # draw an object
        """glLoadIdentity()
        gluPerspective(60, 1, 1, 100)
        glTranslatef(0, -20, -100)
        glRotatef(-120, 1, 1, 1)

        

        # crtaj!
        glTranslatef(self.obj.central_position[0], self.obj.central_position[1], self.obj.central_position[2])
        glRotatef(self.obj.angle, self.obj.axis[0], self.obj.axis[1], self.obj.axis[2])
        self.obj.draw()"""

        glLoadIdentity()
        gluPerspective(60, 1, 1, 100)
        glTranslatef(0, -20, -100)
        glRotatef(-120, 1, 1, 1)
        glColor3f(0, 1, 0)
        self.curve.draw_tangent(self.obj.central_position)

        glFlush()

    def update_object(self, position):
        # set new central position
        self.obj.central_position = position[0]

        # set new axis
        tangent = position[1]
        axis_x = self.obj.starting_direction[1] * tangent[2] - tangent[1] * self.obj.starting_direction[2]
        axis_y = - (self.obj.starting_direction[0] * tangent[2] - tangent[0] * self.obj.starting_direction[2])
        axis_z = self.obj.starting_direction[0] * tangent[1] - self.obj.starting_direction[1] * tangent[0]
        self.obj.axis = [axis_x, axis_y, axis_z]

        # set new angle
        vector_1 = self.obj.starting_direction / np.linalg.norm(self.obj.starting_direction)
        vector_2 = tangent / np.linalg.norm(tangent)

        self.obj.angle = math.degrees(np.arccos(np.clip(np.dot(vector_1, vector_2), -1.0, 1.0)))

    def loop(self):
        # will throw an exception at one point
        while(True):
            time.sleep(1)
            self.update_object(self.curve.get_next_point_and_tangent())
            self.render_scene()
        
    def env_init(self):
        glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
        glutInitWindowSize(600, 500)
        glutInitWindowPosition(400, 200)
        glutInit()
        self.window = glutCreateWindow('')
        glutDisplayFunc(self.render_scene)

    def main(self):
        # load object and curve
        self.obj = Object_loader('aircraft747.obj').load()
        self.curve = Curve(Curve_loader('curve_control_points.txt').load_control_points())

        # init environment
        self.env_init()
        
        # infinite loop
        self.loop()



Main().main()