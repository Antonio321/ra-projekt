from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import pygame
from time import sleep
import random


def load_texture(filename):
    surface = pygame.image.load(filename)
    data = pygame.image.tostring(surface, "RGB")
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, surface.get_width(), surface.get_height(), GL_RGB, GL_UNSIGNED_BYTE, data)
    return texture

class Point:
    def __init__(self, x, y, z) -> None:
        self.x = x
        self.y = y
        self.z = z

class Particle:
    def __init__(self, point, speed, direction) -> None:
        self.point = point
        self.speed = speed
        self.direction = direction
        self.color_c_intensity = 1

    def draw(self):
        glColor3f(self.color_c_intensity, 0, 0)

        glTranslatef(self.point.x, self.point.y, self.point.z)

        glBegin(GL_QUADS)
        glTexCoord2d(0.0, 0.0)
        glVertex3f(-1, -1, 0.0)
        glTexCoord2d(1.0, 0.0)
        glVertex3f(1, -1, 0.0)
        glTexCoord2d(1.0, 1.0)
        glVertex3f(1, 1, 0.0)
        glTexCoord2d(0.0, 1.0)
        glVertex3f(-1, 1, 0.0)
        glEnd()

        glTranslatef(-self.point.x, -self.point.y, -self.point.z)

    def update_position(self):
        self.color_c_intensity -= 0.002
        self.point.x += self.speed * self.direction[0]
        self.point.y += self.speed * self.direction[1]

class ParticleSource:
    def __init__(self, center) -> None:
        self.center = center
        self.particles = []
        self.prev_time = 0
        self.curr_time = 0
        self.frequency = 1.

    def draw(self):
        glColor3f(1, 1, 1)

        glTranslatef(self.center.point.x, self.center.point.y, self.center.point.z)

        glBegin(GL_QUADS)
        glTexCoord2d(0.0, 0.0)
        glVertex3f(-1, -1, 0.0)
        glTexCoord2d(1.0, 0.0)
        glVertex3f(1, -1, 0.0)
        glTexCoord2d(1.0, 1.0)
        glVertex3f(1, 1, 0.0)
        glTexCoord2d(0.0, 1.0)
        glVertex3f(-1, 1, 0.0)
        glEnd()

        glTranslatef(-self.center.point.x, -self.center.point.y, -self.center.point.z)

        self.curr_time = glutGet(GLUT_ELAPSED_TIME)
        if self.curr_time - self.prev_time > 500 * self.frequency:
            self.create_particles()
            self.prev_time = self.curr_time

        deletion_list = []
        for particle in self.particles:
            particle.update_position()
            particle.draw()
            if particle.color_c_intensity <= 0.1:
                deletion_list.append(particle)

        for particle in deletion_list:
            self.particles.remove(particle)

    def update_source_position(self):
        #self.center.point.x += 0.01
        pass


    def create_particles(self):
        x = random.uniform(-0.01, 0.01)
        y = random.uniform(-0.01, 0.01)
        self.particles.append(Particle(Point(self.center.point.x, self.center.point.y, self.center.point.z), 3, (x, y, 0)))

class Main:
    def __init__(self) -> None:
        self.viewpoint = Point(0, 0, 20)
        self.window = None
        self.texture = None
        self.particle_source = ParticleSource(Particle(Point(0, 0, 0), 1, (0,1,0)))

    def display_func(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        glTranslatef(self.viewpoint.x, self.viewpoint.y, -self.viewpoint.z)
        #self.draw_foo_particle()
        self.particle_source.draw()
        glutSwapBuffers()

    def reshape_func(self, w, h):
        glViewport(0, 0, 500, 500)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 500 / 500, 0.1, 150)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glClearColor(0, 0, 0, 0)
        glClear(GL_COLOR_BUFFER_BIT)
        glPointSize(1)
        glColor3f(0, 0, 0)

    def keyboard_func(self, key, x, y):
        if key == b'a':
            self.particle_source.center.point.x -= 0.1
        if key == b'd':
            self.particle_source.center.point.x += 0.1
        if key == b'w':
            self.particle_source.center.point.y += 0.1
        if key == b's':
            self.particle_source.center.point.y -= 0.1
        if key == b'g':
            self.particle_source.frequency += 0.1
        if key == b'f':
            self.particle_source.frequency -= 0.1
        

    def loop(self):
        self.particle_source.update_source_position()
        self.display_func()
        sleep(0.01)

    def main(self):
        glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
        glutInitWindowSize(500, 500)
        glutInitWindowPosition(10, 10)
        glutInit()
        self.window = glutCreateWindow("2. labos")
        glutDisplayFunc(self.display_func)
        glutReshapeFunc(self.reshape_func)
        glutKeyboardFunc(self.keyboard_func)
        glutIdleFunc(self.loop)
        
        self.texture = load_texture("cestica.bmp")

        glBlendFunc(GL_SRC_ALPHA, GL_ONE)
        glEnable(GL_BLEND)
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, self.texture)

        glutMainLoop()

m = Main()
m.main()