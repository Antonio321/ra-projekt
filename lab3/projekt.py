from OpenGL import GL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import pygame
from time import sleep
import random
from math import pi, atan, sin, cos, floor
from typing import List


def load_texture(filename):
    surface = pygame.image.load(filename)
    data = pygame.image.tostring(surface, "RGB")
    texture = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, surface.get_width(), surface.get_height(), GL_RGB, GL_UNSIGNED_BYTE, data)
    return texture


class Point:
    def __init__(self, x, y, z) -> None:
        self.x = x
        self.y = y
        self.z = z


class Particle:
    def __init__(self, point, speed, direction) -> None:
        self.point : Point = point
        self.speed = speed
        self.direction = direction
        self.color_c_intensity = 1
        self.gravity_vector = 1

    def draw(self):
        glColor3f(self.color_c_intensity, 0, 0)

        glTranslatef(self.point.x, self.point.y, self.point.z)

        glBegin(GL_QUADS)
        glTexCoord2d(0.0, 0.0)
        glVertex3f(-1, -1, 0.0)
        glTexCoord2d(1.0, 0.0)
        glVertex3f(1, -1, 0.0)
        glTexCoord2d(1.0, 1.0)
        glVertex3f(1, 1, 0.0)
        glTexCoord2d(0.0, 1.0)
        glVertex3f(-1, 1, 0.0)
        glEnd()

        glTranslatef(-self.point.x, -self.point.y, -self.point.z)

    def update_position(self):
        self.color_c_intensity -= 0.002
        self.point.x += self.speed * self.direction[0]
        self.point.y += self.speed * self.direction[1]

        self.point.y -= self.gravity_vector * 0.01
        self.gravity_vector *= 1.01


class FinishLine:
    def __init__(self) -> None:
        self.center : Point = Point(6, 6, 0)
        self.width = random.uniform(0, 0.5)
        self.height = random.uniform(0, 0.5)
        self.change_coordinates()
        self.color = (0, 1, 0)

    def draw_finish_line(self):
        width = self.width
        height = self.height
        coordinates = (self.center.x, self.center.y)

        glDisable(GL_TEXTURE_2D)
        glPointSize(3)
        """glBegin(GL_POINTS)
        glColor(*self.color)
        
        glVertex3f(coordinates[0] + width, coordinates[1] + height, 0)
        glVertex3f(coordinates[0] + width, coordinates[1] - height, 0)
        glVertex3f(coordinates[0] - width, coordinates[1] + height, 0)
        glVertex3f(coordinates[0] - width, coordinates[1] - height, 0)
        
        glColor(1, 1, 1)
        glEnd()"""



        glBegin(GL_LINES)
        glColor(*self.color)

        glVertex3f(coordinates[0] + width, coordinates[1] + height, 0)
        glVertex3f(coordinates[0] + width, coordinates[1] - height, 0)
        
        glVertex3f(coordinates[0] + width, coordinates[1] - height, 0)
        glVertex3f(coordinates[0] - width, coordinates[1] - height, 0)

        glVertex3f(coordinates[0] - width, coordinates[1] - height, 0)
        glVertex3f(coordinates[0] - width, coordinates[1] + height, 0)

        glVertex3f(coordinates[0] - width, coordinates[1] + height, 0)
        glVertex3f(coordinates[0] + width, coordinates[1] + height, 0)


        glColor(1, 1, 1)
        glEnd()

        
        glPointSize(1)
        glEnable(GL_TEXTURE_2D)


    def check_goal(self, particle : Particle):
        x = particle.point.x
        y = particle.point.y

        width = self.width
        height = self.height
        coordinates = (self.center.x, self.center.y)


        p1 = coordinates[0] + width
        p2 = coordinates[0] - width
        p3 = coordinates[1] + height
        p4 = coordinates[1] - height

        if x > p2 and x < p1 and y < p3 and y > p4:
            return True
        return False

    def change_coordinates(self):
        self.center : Point = Point(random.uniform(-5, 5), random.uniform(-5, 5), 0)
        self.width = random.uniform(0.3, 0.7)
        self.height = random.uniform(0.3, 0.7)

    def update_color(self):
        if self.color[1] != 0 and self.color[0] != 1:
            self.color = (self.color[0] + 0.0015, self.color[1] - 0.0015, 0)

class Score:
    def __init__(self) -> None:
        self.score = 0

    def draw_score(self):
        glDisable(GL_TEXTURE_2D)
        glRasterPos2f(3, 7)  # Brojevi između 0 i 1
        for letter in "SCORE: {}".format(round(self.score, 2)):
            glutBitmapCharacter(fonts.GLUT_BITMAP_TIMES_ROMAN_24, ctypes.c_int(ord(letter)))
        glEnable(GL_TEXTURE_2D)

    def update_score(self, percentage : float):
        self.score += max(round(percentage, 2) * 5, 0)

        return max(round(percentage, 2) * 5, 0)

class ScoreAnnotation:
    def __init__(self, point, percentage) -> None:
        self.starting_location = Point(*point)
        self.point = Point(*point)
        self.score = percentage
        print(self.score)

    def update_position(self):
        self.point.y += 0.01

        if self.point.y - self.starting_location.y > 1:
            return False
        return True

    def draw(self):
        glDisable(GL_TEXTURE_2D)
        glColor(1-self.score / 5, self.score / 5, 0)
        glRasterPos2f(self.point.x, self.point.y)  # Brojevi između 0 i 1
        for letter in "+{}".format(round(self.score, 2)):
            glutBitmapCharacter(fonts.GLUT_BITMAP_TIMES_ROMAN_10, ctypes.c_int(ord(letter)))
        glColor(1, 1, 1)
        glEnable(GL_TEXTURE_2D)


class ParticleSource:
    def __init__(self, center) -> None:
        self.center = center
        self.particles = []
        self.prev_time = 0
        self.curr_time = 0
        self.frequency = 1.
        self.angle = 0.
        self.throwing_strength = 3
        self.finish_lines : List[FinishLine] = [FinishLine()]
        self.score = Score()
        self.annotations : List[ScoreAnnotation] = []

    

    def draw_shooting_line(self):
        glTranslatef(self.center.point.x, self.center.point.y, self.center.point.z)

        x = cos(self.angle)
        y = sin(self.angle)

        glDisable(GL_TEXTURE_2D)
        glPointSize(4)
        glBegin(GL_POINTS)
        for i in range(1, floor(self.throwing_strength)):
            glVertex3f(x * i / 2, y * i / 2, 0)
        glEnd()
        glPointSize(1)
        glEnable(GL_TEXTURE_2D)

        glTranslatef(-self.center.point.x, -self.center.point.y, -self.center.point.z)


    def draw(self):
        glColor3f(1, 1, 1)

        glTranslatef(self.center.point.x, self.center.point.y, self.center.point.z)

        glBegin(GL_QUADS)
        glTexCoord2d(0.0, 0.0)
        glVertex3f(-1, -1, 0.0)
        glTexCoord2d(1.0, 0.0)
        glVertex3f(1, -1, 0.0)
        glTexCoord2d(1.0, 1.0)
        glVertex3f(1, 1, 0.0)
        glTexCoord2d(0.0, 1.0)
        glVertex3f(-1, 1, 0.0)
        glEnd()

        glTranslatef(-self.center.point.x, -self.center.point.y, -self.center.point.z)



        self.score.draw_score()
        self.draw_shooting_line()   

        for finish_line in self.finish_lines:
            finish_line.update_color()
            finish_line.draw_finish_line()


        self.curr_time = glutGet(GLUT_ELAPSED_TIME)
        if self.curr_time - self.prev_time > 3000 * self.frequency:
            self.prev_time = self.curr_time
            self.finish_lines.append(FinishLine())

        deletion_list = []
        finish_line_deletion_list = []
        for particle in self.particles:
            particle.update_position()
            particle.draw()


            for finish_line in self.finish_lines:
                if finish_line.check_goal(particle):
                    #deletion_list.append(particle)
                    score = self.score.update_score(finish_line.color[1])
                    finish_line_deletion_list.append(finish_line)

                    self.annotations.append(ScoreAnnotation((finish_line.center.x, finish_line.center.y, 0), score))
                elif particle.color_c_intensity <= 0.1:
                    deletion_list.append(particle)

        """for particle in deletion_list:
            self.particles.remove(particle)"""

        for finish_line in finish_line_deletion_list:
            self.finish_lines.remove(finish_line)

        annotations_to_delete = []
        for annotation in self.annotations:
            annotation.draw()
            if annotation.update_position() == False:
                annotations_to_delete.append(annotation)

        for annotation in annotations_to_delete:
            self.annotations.remove(annotation)




    def create_particles(self):
        x = 0.01 * cos(self.angle)
        y = 0.01 * sin(self.angle)
        self.particles.append(
            Particle(Point(self.center.point.x, self.center.point.y, self.center.point.z), self.throwing_strength,
                     (x, y, 0)))

    def raise_angle(self):
        self.angle += pi / 24

    def lower_angle(self):
        self.angle -= pi / 24

    def raise_throwing_strength(self):
        self.throwing_strength += 0.5

    def lower_throwing_strength(self):
        self.throwing_strength -= 0.5


class Main:
    def __init__(self) -> None:
        self.viewpoint = Point(0, 0, 20)
        self.window = None
        self.texture = None
        self.particle_source = ParticleSource(Particle(Point(0, 0, 0), 1, (0, 1, 0)))

    def display_func(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        glTranslatef(self.viewpoint.x, self.viewpoint.y, -self.viewpoint.z)
        self.particle_source.draw()
        glutSwapBuffers()

    def reshape_func(self, w, h):
        glViewport(0, 0, 500, 500)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 500 / 500, 0.1, 150)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glClearColor(0, 0, 0, 0)
        glClear(GL_COLOR_BUFFER_BIT)
        glPointSize(1)
        glColor3f(0, 0, 0)

    def keyboard_func(self, key, x, y):
        if key == b'a':
            self.particle_source.center.point.x -= 0.1
        if key == b'd':
            self.particle_source.center.point.x += 0.1
        if key == b'w':
            self.particle_source.center.point.y += 0.1
        if key == b's':
            self.particle_source.center.point.y -= 0.1
        if key == b'g':
            self.particle_source.frequency += 0.1
        if key == b'f':
            self.particle_source.frequency -= 0.1
        if key == b'p':
            self.particle_source.create_particles()
        if key == b'h':
            self.particle_source.raise_angle()
        if key == b'j':
            self.particle_source.lower_angle()
        if key == b'n':
            self.particle_source.raise_throwing_strength()
        if key == b'm':
            self.particle_source.lower_throwing_strength()

    def loop(self):
        self.display_func()
        sleep(0.01)

    def main(self):
        glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
        glutInitWindowSize(500, 500)
        glutInitWindowPosition(10, 10)
        glutInit()
        self.window = glutCreateWindow("Projekt")
        glutDisplayFunc(self.display_func)
        glutReshapeFunc(self.reshape_func)
        glutKeyboardFunc(self.keyboard_func)
        glutIdleFunc(self.loop)

        self.texture = load_texture("cestica.bmp")

        glBlendFunc(GL_SRC_ALPHA, GL_ONE)
        glEnable(GL_BLEND)
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, self.texture)

        glutMainLoop()


m = Main()
m.main()
